var fs = require('fs');

var casper = require('casper').create({
    verbose: true,
    logLevel: "debug",
    pageSettings: {
        userAgent: "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:23.0) Gecko/20130404 Firefox/23.0",
        loadImages: false,        // do not load images
        loadPlugins: false         // do not load NPAPI plugins (Flash, Silverlight, ...)
    }
});

// Global variables
var jobs = [];


// CLI parameters parsing
var url = casper.cli.get(0);
casper.echo("Fetching " + url);

var urlName = casper.cli.get(1);
casper.echo("Known as " + urlName);

var uid = String(casper.cli.get(2));
casper.echo("UID is " + uid);

var output = casper.cli.get(3) + '/' + uid;
casper.echo("Output folder is " + output);



// Output file
var currentTime = new Date();var month = currentTime.getMonth() + 1;var day = currentTime.getDate();var year = currentTime.getFullYear();
var outputFile = output + "/" + urlName + "-" + year + "-" + month + "-" + day + ".json";

// Enqueue URL
function enqueueURL(uid, urlName, url) {
    var result = JSON.parse(__utils__.sendAJAX('http://localhost:8080/enqueue/mondoconv.it/' + urlName, 'POST', JSON.stringify({
        url: url,
        command: 'item-variations',
        uid: uid
    }), false)); 
    if (result != 'ok')
        __utils__.log('Cannot send URL to server: ' + url, 'error');    
}

// Scrape data routine
function getJobs(uid, urlName, enqueueFunction) {
    var rows = document.querySelectorAll('li.product-item');
    var jobs = [];
    var urls = []

    for (var i = 0, row; row = rows[i]; i++) {
        var job = {};
        job['taskUid'] = uid;
        job['url'] = row.querySelector('a.product').getAttribute("href");
        job['code'] = row.querySelector('div.product-item-mc-details div.mc-sku').innerText.replace('Codice: ','');
        job['name'] = row.querySelector('a.product-item-link span.name').innerText;
        job['color'] = row.querySelector('div.product-item-mc-details div.mc-colore').innerText;
        job['description'] = row.querySelector('div.product-item-mc-details div.mc-descrizione').innerText;
        try {
            job['dimension'] = row.querySelector('div.product-item-mc-details div.mc-dimensioni').innerText;
        } catch (e) {
            job['dimension'] = ''
         }
        job['price'] = parseInt(row.querySelector('span.price-wrapper span.price').innerText.replace('.',''), 10);
        try {
            job['oldPrice'] = parseInt(row.querySelector('span[data-price-type="oldPrice"]').innerText.replace('.',''), 10);
        } catch (e) { 
            job['oldPrice'] = 0
        }
        enqueueFunction(uid, urlName, job['url'])
        jobs.push(job);
    }
    
    
    return jobs;
}

var processPage = function () {
    writeToFile(this.evaluate(getJobs,{uid: uid, urlName: urlName, enqueueFunction: enqueueURL}));
    return terminate.call(casper);
};

var writeToFile = function (data) {
    if (data != undefined && data.length > 0) {
        var jsonData = '';
        for (var i = 0, row; row = data[i]; i++) {
            jsonData += JSON.stringify(row)+"\n"
            
        }
        fs.write(outputFile, jsonData, 'a');
    }
}

var terminate = function () {
    this.echo("Enjoy!").exit();
};

casper.start(url, function() {
    this.scrollToBottom();  
});
casper.waitForSelector('footer', processPage, terminate);
casper.run();

